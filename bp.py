import numpy as np
import itertools
from scipy.special import logsumexp, expit, softmax, logit
from utils import log_bern

NEGATIVE = -1000
EPS = 1e-3


class SP:
    def __init__(self, hps, f_eval):
        self.m, m = hps.m, hps.m
        self.n, n = hps.n, hps.n
        self.n_b, n_b = hps.n_b, hps.n_b

        self.f_eval = f_eval
        self.maxsat = hps.maxsat
        if self.maxsat:
            def energy_wrapper(beta=1.):
                def energy(*args, **kwargs):
                    return -(1. - f_eval(*args, **kwargs)) * beta

                return energy

            self.f_eval = energy_wrapper(1.)

        self.maxprod = hps.maxprod
        self.gamma = hps.gamma
        self.p = 1. if hps.maxprod else 1.

        if hps.random_init_props:
            self.f2var = softmax(np.random.randn(m, n, n_b) * .1, -1)
            self.var2f = softmax(np.random.randn(m, n, n_b) * .1, -1)
            # self.f2var = np.zeros((m, n, n_b))
        else:
            self.f2var_tmp = np.zeros((m, n, n_b))
            self.var2f = np.random.rand(m, n, n_b)
            # self.var2f[:, :, n_b // 2] = 1.
            self.var2f = self.var2f / self.var2f.sum(-1, keepdims=True)
            self.f2var = np.zeros((m, n, n_b))
        self.logits = np.linspace(-3, 3, hps.n_b)
        self.bin_probs = expit(self.logits)
        self.pad_length_v2f = (m - 1) * (n_b - 1) + 1 - n_b
        self.bel = None
        self.map = None
        if hps.exhaustive:
            raise NotImplementedError('Currently not supported!')
        else:
            self.sample = None
            self.update_f2var = self.update_f2var_stoch

    def update_var2f(self):

        def fftifft(pad_size, calc_bel=False):
            s_0 = (1 - self.bin_probs) * self.f2var
            s_1 = self.bin_probs * self.f2var
            # padding
            s_0 = s_0**self.p
            s_1 = s_1**self.p



            s_0 = np.pad(s_0, [(0, 0), (0, 0), (0, pad_size)], 'constant',
                         constant_values=0.)
            s_1 = np.pad(s_1, [(0, 0), (0, 0), (0, pad_size)], 'constant',
                         constant_values=0.)
            s_0 = np.fft.fft(s_0)
            s_1 = np.fft.fft(s_1)

            s_0_prod = np.prod(s_0, 0, keepdims=True)
            s_1_prod = np.prod(s_1, 0, keepdims=True)
            if calc_bel:
                s_0 = 1.
                s_1 = 1.

            var2f = np.real(np.fft.ifft(s_0_prod / s_0 + s_1_prod / s_1))
            low, mid, high = np.split(var2f, [pad_size // 2,
                                              -pad_size // 2], 2)
            mid[:, :, 0] = mid[:, :, 0] + low.sum(2)
            mid[:, :, -1] = mid[:, :, -1] + high.sum(2)
            return mid / mid.sum(-1, keepdims=True)

        mid = fftifft(self.pad_length_v2f)
        mid = mid**(1./self.p)
        self.var2f = self.var2f * (1 - self.gamma) + mid * self.gamma
        bel = fftifft(self.pad_length_v2f + self.n_b - 1, True)
        k_max = bel.argmax(-1)
        certainity = bel[0, np.arange(self.n), k_max]
        print(f'certainity {certainity}')
        # print(f'belief {bel}')
        self.bel = self.bin_probs[k_max]
        self.map = (self.bel > .5).astype(float)

    def update_m(self, m, up_val, gamma=None):
        if gamma is None:
            gamma = self.gamma
        return m * (1 - gamma) + up_val * gamma

    def normalize_f2var(self):
        # self.f2var_tmp = self.f2var_tmp / self.f2var_tmp.sum(-1, keepdims=True)
        self.f2var = self.update_m(self.f2var, self.f2var_tmp, .8)
        self.f2var_tmp = np.zeros((self.m, self.n, self.n_b))

    def update_f2var_stoch(self, x, y, ind):
        # TODO: support multi-samples
        # draw samples from SP
        l = 6
        for _ in range(l):
            var2f_k = (np.random.rand(self.n, 1) >=
                       np.cumsum(self.var2f[ind], -1)).sum(-1)

            var2f_p = self.bin_probs[var2f_k]
            p_0_tilde, p_1_tilde, p_hat = self.bp_f2var_stoch(x, y, var2f_p)
            if self.maxprod:
                p_0 = np.maximum(p_0_tilde, p_1_tilde)
            else:
                p_0 = p_0_tilde + p_1_tilde
            k = np.digitize(p_hat, self.bin_probs, True)
            k[k == self.n_b] = self.n_b - 1
            self.f2var[ind][np.arange(self.n), k] = self.update_m(self.f2var[ind][np.arange(self.n), k], p_0)

    def bp_f2var_stoch(self, x, y, var2f):

        l = 10
        sample_1, sample_0 = np.split((np.random.rand(l, 2 * self.n, self.n)
                                       < var2f).astype(float), 2, axis=1)
        sample_0[:, np.arange(self.n), np.arange(self.n)] = 0.
        sample_1[:, np.arange(self.n), np.arange(self.n)] = 1.

        m_1 = self.f_eval(sample_1, x, y).squeeze(2)
        m_0 = self.f_eval(sample_0, x, y).squeeze(2)
        if self.maxprod:
            p = log_bern(sample_1, var2f)
            p = p.sum(-1) - p[:, np.arange(self.n), np.arange(self.n)]
            if self.maxsat:
                m_1 = m_1 + p
            else:
                m_1 = m_1 * p

            p = log_bern(sample_0, var2f)
            p = p.sum(-1) - p[:, np.arange(self.n), np.arange(self.n)]
            if self.maxsat:
                m_0 = m_0 + p
            else:
                m_0 = m_0 * p

            m_1[m_1 == 0.] = NEGATIVE
            m_0[m_0 == 0.] = NEGATIVE
            m_1 = np.max(m_1, 0)
            m_0 = np.max(m_0, 0)
            return np.exp(m_0), np.exp(m_1), expit(m_1 - m_0)
            # self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + expit(m_1 - m_0) * self.gamma

        else:
            if self.maxsat:
                m_1 = logsumexp(m_1, 0)
                m_0 = logsumexp(m_0, 0)
                return np.exp(m_0), np.exp(m_1), expit(m_1 - m_0)
                # self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + \
                #                   expit(m_1 - m_0) * self.gamma
            else:

                m_1 = np.mean(m_1, 0)
                m_0 = np.mean(m_0, 0)

                return m_0, m_1, (m_1 + EPS) / (m_1 + m_0 + EPS)
                # self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + \
                #                   (m_1 + EPS) / (m_1 + m_0 + EPS) * self.gamma


class BP:
    def __init__(self, hps, f_eval):
        self.m, m = hps.m, hps.m
        self.n, n = hps.n, hps.n

        self.f_eval = f_eval
        self.maxsat = hps.maxsat
        if self.maxsat:
            def energy_wrapper(beta=1.):
                def energy(*args, **kwargs):
                    return -(1. - f_eval(*args, **kwargs)) * beta

                return energy

            self.f_eval = energy_wrapper(1.)

        self.maxprod = hps.maxprod
        self.gamma = hps.gamma

        if hps.random_init_props:
            self.f2var = expit(np.random.randn(m, n) * .1)
            self.var2f = expit(np.random.randn(m, n) * .1)
        else:
            self.f2var = np.ones((m, n)) * .5
            self.var2f = np.ones((m, n)) * .5
        self.bel = None
        self.map = None
        if hps.exhaustive:
            self.sample = np.array(list(itertools.product([0, 1],
                                                          repeat=n))).astype(float)
            self.update_f2var = self.update_f2var_exh
            self.mask_1 = np.zeros((n, m))
            for i in range(n):
                self.mask_1[i] = (self.sample[..., i] == 1.)
        else:
            self.sample = None
            self.update_f2var = self.update_f2var_stoch

    def update_f2var_exh(self, x, y, ind):
        oper = np.max if self.maxprod else logsumexp
        f_bel = log_bern(self.sample, self.var2f[ind])
        m = (f_bel.sum(-1, keepdims=True) - f_bel)
        if self.maxsat:
            m = (m + self.f_eval(self.sample, x, y)).T
        else:
            m = (m * self.f_eval(self.sample, x, y)).T
        m_1 = m * self.mask_1
        m_0 = m * (1 - self.mask_1)
        m_1[m_1 == 0.] = NEGATIVE
        m_0[m_0 == 0.] = NEGATIVE
        m_1 = oper(m_1, 1)
        m_0 = oper(m_0, 1)
        self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + expit(m_1 - m_0) * self.gamma

    def update_f2var_stoch(self, x, y, ind):

        l = 10
        sample_1, sample_0 = np.split((np.random.rand(l, 2 * self.n, self.n)
                                       < self.var2f[ind]).astype(float), 2, axis=1)
        sample_0[:, np.arange(self.n), np.arange(self.n)] = 0.
        sample_1[:, np.arange(self.n), np.arange(self.n)] = 1.

        m_1 = self.f_eval(sample_1, x, y).squeeze(2)
        m_0 = self.f_eval(sample_0, x, y).squeeze(2)
        if self.maxprod:
            p = log_bern(sample_1, self.var2f[ind])
            p = p.sum(-1) - p[:, np.arange(self.n), np.arange(self.n)]
            if self.maxsat:
                m_1 = m_1 + p
            else:
                m_1 = m_1 * p

            p = log_bern(sample_0, self.var2f[ind])
            p = p.sum(-1) - p[:, np.arange(self.n), np.arange(self.n)]
            if self.maxsat:
                m_0 = m_0 + p
            else:
                m_0 = m_0 * p

            m_1[m_1 == 0.] = NEGATIVE
            m_0[m_0 == 0.] = NEGATIVE
            m_1 = np.max(m_1, 0)
            m_0 = np.max(m_0, 0)
            self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + expit(m_1 - m_0) * self.gamma

        else:
            if self.maxsat:
                m_1 = logsumexp(m_1, 0)
                m_0 = logsumexp(m_0, 0)
                self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + \
                                  expit(m_1 - m_0) * self.gamma
            else:

                m_1 = np.mean(m_1, 0)
                m_0 = np.mean(m_0, 0)

                self.f2var[ind] = self.f2var[ind] * (1 - self.gamma) + \
                                  (m_1 + EPS) / (m_1 + m_0 + EPS) * self.gamma

    def update_var2f(self):
        logf2var_1 = np.log(self.f2var + EPS)
        m_1 = logf2var_1.sum(0, keepdims=True)
        logf2var_0 = np.log(1 - self.f2var + EPS)
        m_0 = logf2var_0.sum(0, keepdims=True)
        self.bel = expit(m_1 - m_0)
        self.map = (m_1 > m_0).astype(float)
        m_1 = m_1 - logf2var_1
        m_0 = m_0 - logf2var_0
        self.var2f = self.var2f * (1 - self.gamma) + expit(m_1 - m_0) * self.gamma

    def normalize_f2var(self):
        pass
