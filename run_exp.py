import argparse
import utils
import bp
import numpy as np
np.random.seed(10)

if __name__ == '__main__':

    # TODO: replace with proper argparse
    class HP:
        pass

    hps = HP
    hps.model = 'sp'
    hps.n_iters = 60
    hps.n = 20  # number of variables
    hps.n_b = 101  # number of bins for SP
    hps.maxprod = True
    hps.multi_layer = False
    hps.exhaustive = False
    hps.maxsat = False
    hps.n_data = 2**4
    hps.random_init_props = False
    hps.gamma = .2  # damping factor
    hps.m = hps.n_data if hps.n_data and hps.n_data <= 2**hps.n \
        else 2**hps.n  # number of clauses
    d = utils.generate_data(hps)
    X, Y = d.X, d.Y
    if hps.model.lower() == 'bp':
        net = bp.BP(hps, utils.f_eval)
    elif hps.model.lower() == 'sp':
        net = bp.SP(hps, utils.f_eval)
    else:
        raise NotImplementedError()
    for _ in range(hps.n_iters):
        for ind, (x, y) in enumerate(zip(X, Y)):
            net.update_f2var(x, y, ind)
        net.update_var2f()
        # net.normalize_f2var()
        # print(f'marginal \n {net.bel}')
        print(f'MAP \n {net.map}')
        acc = utils.f_eval(net.map, X, Y).sum() / X.shape[0]
        print(f'accuracy is {acc}')
        if acc == 1.:
            break
        # print(f'\nf2var\n {net.f2var}\n var2f\n {net.var2f}')
        print('**'*20)
    if not hps.multi_layer:
        print(f'real w is\n {(d.w.reshape(1, -1)+1)/2}')

