import numpy as np
import utils
np.random.seed(6)


class HP:
    pass


def accuracy(w, X, Y):
    return ((X @ w >= 0) * 2 - 1 == Y).astype(float)


hps = HP
hps.n_iters = 60
hps.multi_layer = False
hps.n = 60  # number of variables
hps.n_data = 2 ** 10
d = utils.generate_data(hps)
hps.multi_layer = False

w = np.random.choice([1., -1], (hps.n, 1))
ns = np.arange(hps.n)
step = 0.
for _ in range(hps.n_iters):
    np.random.shuffle(ns)
    for i in ns:
    # for i in range(hps.n):
        ac = accuracy(w, d.X, d.Y).mean()
        print(ac)
        w[i] = - w[i]
        ac2 = accuracy(w, d.X, d.Y).mean()
        p = np.exp((ac2-ac)*.1)
        if p<1:
            if np.random.rand(1) < p:
                w[i] = - w[i]

    print(w.reshape(-1))
    print('*'*30)



