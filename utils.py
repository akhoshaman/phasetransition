import os
import numpy as np
import itertools
import logging


def generate_data(hps, random_w=True):
    n = hps.n
    n_data = hps.n_data
    multi_layer = hps.multi_layer
    class Data:
        pass

    d = Data()
    d.w = []
    if random_w is None:
        w = np.array([1, 1, -1]).reshape(n, 1)
    else:
        w = np.random.choice([1., -1], (n, 1))
    d.w.append(w)
    if n_data is None:
        x = np.array(list(itertools.product([0, 1], repeat=n))).astype(float)
    else:
        x = np.random.binomial(1, .5, (n_data, n))
    if multi_layer:

        h = x
        for _ in range(2):
            w_layer = np.random.choice([1., -1], (n, n))
            h = ((h @ w_layer) >= 0.).astype(float)
            d.w.append(w_layer)
        y = ((h @ w) >= 0.).astype(float) * 2 - 1

    else:
        y = ((x @ w) >= 0.).astype(float)*2-1

    d.X = x
    d.Y = y
    d.w = w
    return d


def f_eval(w_bin, x, y):
    return ((((2*w_bin-1)*x).sum(-1, keepdims=True) >= 0)*2-1 == y).astype(float)


def log_bern(w, p, eps=1e-7):
    return w * np.log(p + eps) + (1 - w) * np.log(1. - p + eps)

def calc_accuracy(x, y, w_bin, f_eval):
    return f_eval(w_bin, x, y).sum()/y.shape[0]

def makedirs(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)


def get_logger(logpath, filepath, package_files=[], displaying=True, saving=True, debug=False):
    logger = logging.getLogger()
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logger.setLevel(level)
    if saving:
        info_file_handler = logging.FileHandler(logpath, mode="a")
        info_file_handler.setLevel(level)
        logger.addHandler(info_file_handler)
    if displaying:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(level)
        logger.addHandler(console_handler)
    logger.info(filepath)
    with open(filepath, "r") as f:
        logger.info(f.read())

    for f in package_files:
        logger.info(f)
        with open(f, "r") as package_f:
            logger.info(package_f.read())

    return logger


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class RunningAverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, momentum=0.99):
        self.momentum = momentum
        self.reset()

    def reset(self):
        self.val = None
        self.avg = 0

    def update(self, val):
        if self.val is None:
            self.avg = val
        else:
            self.avg = self.avg * self.momentum + val * (1 - self.momentum)
        self.val = val


def inf_generator(iterable):
    """Allows training with DataLoaders in a single infinite loop:
        for i, (x, y) in enumerate(inf_generator(train_loader)):
    """
    iterator = iterable.__iter__()
    while True:
        try:
            yield iterator.__next__()
        except StopIteration:
            iterator = iterable.__iter__()


